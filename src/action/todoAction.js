export const ADD_TODO = "ADD_TODO";
export const FETCH_TODO = "FETCH_TODO";

export const add = taskName => {
  return {
    type: ADD_TODO,
    payload: taskName
    // payload: { taskName }
  };
};

export const fetch = () => {
  return {
    type: FETCH_TODO
  };
};
