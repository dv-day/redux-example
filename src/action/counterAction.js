export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";

export const increase = (value) => {
  const action = {
    type: "INCREMENT",
    payload: value
  };
  return action;
};

export const decrease = (value) => {
  const action = {
    type: "DECREMENT",
    payload: value
  };
  return action;
};
