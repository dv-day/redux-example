import React from "react";
import "./App.css";
import Counter from "./component/Counter";
import CounterRedux from "./component/CounterRedux";
import TodoRedux from "./component/TodoRedux";

function App() {
  return (
    <div className="App">
      <Counter></Counter>
      <hr></hr>
      <CounterRedux></CounterRedux>
      <hr></hr>
      <TodoRedux></TodoRedux>
      <hr></hr>
    </div>
  );
}

export default App;
