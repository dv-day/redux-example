import { INCREMENT } from "../action/counterAction";
import { DECREMENT } from "../action/counterAction";

const initialCount = 0;

const counterReducer = (state = initialCount, action) => {
  switch (action.type) {
    case INCREMENT:
      return state + action.payload;
    case DECREMENT:
      return state <= 0 ? 0 : state - action.payload;
    default:
      return state;
  }
};

export default counterReducer;
