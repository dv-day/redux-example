import { combineReducers } from "redux";
import counterReducer from "./counterReducer";
import todoReducer from './todoReducer'

export const rootReducer = combineReducers({
  counts : counterReducer,
  todos : todoReducer
});
