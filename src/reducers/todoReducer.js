import { ADD_TODO, FETCH_TODO } from "../action/todoAction";

const initialTodos = [];

const todoReducer = (state = initialTodos, action) => {
  switch (action.type) {
    case ADD_TODO:
      //   return [action.payload, ...state];
      const newTodo = {
        taskName: action.payload
      };
      return [newTodo, ...state];
      case FETCH_TODO:
        const mockTodo= [
            { taskName: "wow"},
            { taskName: "what"},
            { taskName: "the"},
            { taskName: "fawk"}
        ]
          return mockTodo
    default:
      return state;
  }
};

export default todoReducer;
