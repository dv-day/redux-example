import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { add } from "../action/todoAction";

const TodoForm = () => {
  const [todoName, setTodoName] = useState("");
  const dispatch = useDispatch();

  const onClickHandler = () => {
    dispatch(add(todoName));
    setTodoName("");
  };
  return (
    <div>
      <input
        value={todoName}
        onChange={e => setTodoName(e.target.value)}
      ></input>
      <button onClick={onClickHandler}>ADD</button>
    </div>
  );
};

export default TodoForm;
