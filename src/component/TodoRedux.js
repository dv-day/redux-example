import React, { useEffect } from "react";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import { useDispatch } from "react-redux";
import { fetch } from "../action/todoAction";

const TodoRedux = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetch());
  }, []);
  
  return (
    <div>
      <TodoForm></TodoForm>
      <TodoList></TodoList>
    </div>
  );
};
export default TodoRedux;
