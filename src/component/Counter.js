import React, { useState } from "react";

const Counter = () => {
  const [counts, setCounts] = useState(0);
  return (
    <div>
      Counts ={counts}
      <div>
        <button onClick={() => setCounts(counts - 1)}>+</button>
        <button onClick={() => setCounts(counts + 1)}>+</button>
      </div>
    </div>
  );
};
export default Counter;
