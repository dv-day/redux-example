import React from "react";
import { connect } from "react-redux";
import { decrease, increase } from "../action/counterAction";
import { Button } from "antd";
import { bindActionCreators } from "redux";
const ButtonGroup = Button.Group;

const CounterRedux = props => {
  // const counts = useSelector(state => state.counterReducer);
  // const dispatch = useDispatch();

  const { counts, increase, decrease } = props;
  return (
    <div>
      <h1 style={{ fontSize: "200px", fontFamily:"impact" }}>{counts == 0 ? "0" : counts}</h1>

      <div>
        <ButtonGroup>
          <Button type="danger" onClick={() => decrease(1)}>
            -
          </Button>
          <Button type="primary" onClick={() => increase(1)}>
            +
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    counts: state.counts
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ increase, decrease }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(CounterRedux);
