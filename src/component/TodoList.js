import React from "react";
import { useSelector } from "react-redux";

const TodoList = () => {
  const todos = useSelector(state => state.todos);
  return (
    <div>
      
      
      {todos.map((todo) => {
        return <p>{todo.taskName}</p>;
      })}
    </div>
  );
};
export default TodoList;
